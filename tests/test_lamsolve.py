"""Integration tests for lamsolve module."""

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

import lamsolve


def test_tyson():
    """Call main solve function on Tyson's cell cycle data."""
    csv_file = os.path.join(os.path.dirname(__file__), "tyson.csv")
    ode_file = os.path.join(os.path.dirname(__file__), "tyson.ode")
    assert lamsolve.solve(csv_file, ode_file) == "Optimal"
