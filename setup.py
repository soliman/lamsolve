"""Solving a linear program for parameter search in a Mass-Action system."""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="lamsolve",
    version="0.0.1",
    author="Sylvain Soliman",
    author_email="Sylvain.Soliman@inria.fr",
    description="Parameter search for a Mass-Action model in LP",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.inria.fr/soliman/lamsolve",
    license="MIT",
    install_requires=["pulp"],
    tests_requires=["pytest"],
    python_requires=">=3",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
