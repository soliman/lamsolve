.PHONY: init test qiao reqiao

init:
	pip install -e .

test:
	py.test -s tests

qiao.csv qiao.ode:
	echo "load(library:examples/mapk/Qiao07plos.bc). numerical_simulation(time:200). export_table(qiao.csv). export_ode(qiao.ode)." | biocham

newqiao.ode: qiao.csv qiao.ode lamsolve/lamsolve.py
	python3 -m lamsolve -o newqiao.ode qiao.ode qiao.csv

newqiao.csv: newqiao.ode
	echo "numerical_simulation(time:200). export_table($@)." | biocham $<

qiao: qiao.csv newqiao.csv
	./plotcsv.sh qiao.csv
	./plotcsv.sh newqiao.csv

qiao_full.bc: lamsolve/util.py
	python3 $< > $@
	sed -e 's/\*\(.*\)^2 for \1=>/*\1^2 for \1=[\1]=>/' -i'' $@

qiao_full.ode: qiao_full.bc
	echo "export_ode($@)." | biocham $<

newqiao_full.ode: qiao.csv qiao_full.ode lamsolve/lamsolve.py
	python3 -m lamsolve -o newqiao_full.ode qiao_full.ode qiao.csv

newqiao_full.csv newqiao_full.bc: newqiao_full.ode
	echo "export_biocham(newqiao_full.bc). numerical_simulation(time: 200). export_table(newqiao_full.csv)." | biocham $<
	sed -e 's/\*\(.*\)^2 for \1=>/*\1^2 for \1=[\1]=>/' -i'' newqiao_full.bc

newqiao: newqiao_full.csv
	./plotcsv.sh newqiao_full.csv
