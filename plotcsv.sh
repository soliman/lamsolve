#!/bin/bash

# uncomment first line if necessary
sed '1s/^#//' "$1" > "$1.new"
cols=$(head -1 "$1"|tr ',' ' '|wc -w)

cat << EOF > "$1.plot"
set datafile separator ","
set key autotitle columnheader
set termoption noenhanced
set termoption linewidth 2
set termoption fontscale 0.8
set key outside reverse
set border back linecolor rgb "#808080"
set format "%.5g"
set style data lines
set datafile separator ","
set xlabel "Time"
plot for [i=2:int($cols)] "$1.new" using 1:i with lines
EOF

gnuplot -p "$1.plot"
rm -f "$1.plot" "$1.new"
