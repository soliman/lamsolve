"""Read a CSV file and solve the LP for the values of Mass-Action parameters."""

from functools import reduce
from operator import mul
from sys import stdout
from typing import List, Optional, Tuple

from pulp import LpProblem, LpStatus, LpVariable, lpSum

from .util import Header, Point, Series, read_data

Parameters = List[str]
# degrees of variables
Monomial = List[int]
# sign, parameter and monomial
Derivative = List[Tuple[int, str, Monomial]]
Ode = Tuple[int, Derivative]


def parse_ode(ode: str, header: Header) -> Ode:
    """Parse an ode string into our internal representation."""
    molecule = ode.split("/")[0][1:]
    body = ode.split(" = ")[1]
    # FIXME will choke on molecule names that include a '-'
    posneg = map(lambda x: x.split("-"), body.split("+"))
    positives = []
    negatives = []
    for p in posneg:
        if p[0] != "":
            positives.append(p[0])
        negatives.extend(p[1:])
    monomials = [
        (1, k, m) for k, m in map(lambda x: parse_monomial(x, header), positives)
    ]
    monomials.extend(
        [(-1, k, m) for k, m in map(lambda x: parse_monomial(x, header), negatives)]
    )
    return header.index(molecule), monomials


def parse_monomial(mono: str, header: Header) -> Tuple[str, Monomial]:
    """Parse a monomial string into our internal representation."""
    things = mono.split("*")
    powers = [0] * len(header)
    for thing in things[1:]:
        index = thing.find("^")
        if index > -1:
            thing, spower = thing.split("^")
            power = int(spower)
        else:
            power = 1
        powers[header.index(thing)] += power
    return things[0], powers


def evaluate(expr: Derivative, variables, data: Point):
    """Evaluate a derivative at a given point.

    return an Lp expression
    """
    return lpSum(
        [
            variables[param]
            * float(sign)
            * reduce(mul, (data[i] ** power for i, power in enumerate(mono)))
            for sign, param, mono in expr
        ]
    )


def add_bound_constraints(
    model: LpProblem, ode: Ode, variables, data: Series, epsilon: float
):
    """Add bounds on parameters at a data point.

    Enforces that the 'real' derivative at some point is between the measured
    derivative in the preceding and next intervals, with a slack of epsilon
    """
    prevp = data[0]
    current = data[1]
    variable, body = ode
    for nextp in data[2:]:
        # compute observed derivative between prev and current
        dx1 = (current[variable] - prevp[variable]) / (current[0] - prevp[0])
        # compute observed derivative between current and next
        dx2 = (nextp[variable] - current[variable]) / (nextp[0] - current[0])
        down, up = min(dx1, dx2), max(dx1, dx2)
        dx = evaluate(body, variables, current)
        # true derivative should lie between both observed derivatives
        if down - up > epsilon:
            model += dx >= down
            model += dx <= up
        else:
            model += dx + epsilon >= down
            model += dx - epsilon <= up
        prevp, current = current, nextp


def read_odes_as_strings(odefile: str) -> List[str]:
    """Read an XPP ode file and return lines that look like odes."""
    odes = []
    with open(odefile, "r") as f:
        lines = f.read().splitlines()
    for line in lines:
        if line.find("/dt = ") > -1 and not line.endswith("/dt = 0"):
            odes.append(line)
    return odes


def solve(
    datafiles: List[str],
    odefile: str,
    outfile=stdout,
    debug: bool = False,
    upperbound: Optional[float] = None,
    epsilon=1e-4,
) -> LpStatus:
    """Solve the problem associated with the file/params/odes."""
    header, data = read_data(datafiles)
    odes = read_odes_as_strings(odefile)
    params = []
    equations = [parse_ode(ode, header) for ode in odes]
    for _var, body in equations:
        for _sign, par, _monomial in body:
            params.append(par)
    params = list(set(params))
    # params.append("eps")
    # defaults to LpMinimize
    model = LpProblem()
    variables = LpVariable.dicts("param", params, lowBound=0, upBound=upperbound)
    # objective function
    model += lpSum(variables.values())
    # model += variables["eps"]
    for eq in equations:
        for series in data:
            add_bound_constraints(model, eq, variables, series, epsilon)
    if debug:
        model.writeLP("lamsolve.lp")
    model.solve()
    print_result(model, outfile, odefile)
    if outfile != stdout:
        print_xpp_result(outfile, variables, header, equations, data[0][0])
    return LpStatus[model.status]


def print_result(model: LpProblem, outfile, odefile):
    """Display the result and generate ODE file."""
    print("Status:", LpStatus[model.status])
    for v in model.variables():
        # remove param_ from the name
        if v.name == "param_eps":
            print("par", v.name[6:], "=", v.varValue)
        else:
            print("par", v.name[6:], "=", v.varValue, file=outfile)


def print_xpp_result(outfile, variables, header: Header, odes: List[Ode], init: Point):
    """Make a full XPP file after the parameter values."""
    for var, name in enumerate(header[1:]):
        print(f"init {name} = {init[var + 1]}", file=outfile)
    for var, body in odes:
        print(f"d{header[var]}/dt = ", end="", file=outfile)
        print_ode(outfile, variables, header, body)
    print("done", file=outfile)


def print_ode(outfile, variables, header: Header, body: Derivative):
    """Print a sum of monomials if parameter is non zero."""
    print(
        " + ".join(
            ("-" if sign < 0 else "") + f"{par}*" + monomial_to_str(mono, header)
            for sign, par, mono in body
            if variables[par].varValue != 0.0
        ),
        file=outfile,
    )


def monomial_to_str(mono: Monomial, header: Header) -> str:
    """Produce a string representation of a monomial."""
    result = "*".join(
        header[i] if degree == 1 else header[i] + f"^{degree}"
        for i, degree in enumerate(mono)
        if degree > 0
    )
    if result:
        return result
    return "1"
