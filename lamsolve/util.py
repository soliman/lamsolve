"""Variuous utility functions."""

from csv import reader
from sys import argv
from typing import List, Tuple

Header = List[str]
Point = List[float]
Series = List[Point]
Data = List[Series]


def gen_biocham_model(molecules: List[str]):
    """Write all reactions with up to three molecules."""
    gen_double_reactions(molecules)
    gen_triple_reactions(molecules)


def gen_double_reactions(molecules: List[str]):
    """Write all reactions with one or two molecules."""
    mols = molecules.copy()
    mols.append("_")
    counter = 0
    for m in mols:
        for n in mols:
            if n != m:
                for o in mols:
                    print(f"MA(kd{counter}) for {m} + {o} => {n} + {o}.")
                    counter += 1


def gen_triple_reactions(molecules: List[str]):
    """Write all reactions with three molecules."""
    counter = 0
    for m in molecules:
        for n in molecules:
            if n != m:
                for o in molecules:
                    if o != n and o != m:
                        if n < o:
                            print(f"MA(kt{counter}) for {m} => {n} + {o}.")
                            counter += 1
                        if m < n:
                            print(f"MA(kt{counter}) for {m} + {n} => {o}.")
                            counter += 1


# might be better with pandas
def read_data(files: List[str]) -> Tuple[Header, Data]:
    """Read biocham simulation data from a CSV file and return it."""
    header: Header = []
    data: Data = []
    for file in files:
        time_series: Series = []
        with open(file, "r", newline="") as f:
            for row in reader(f):
                if header == []:
                    # remove first '#' character
                    row[0] = row[0][1:]
                    header = row
                else:
                    time_series.append(list(map(float, row)))
        data.append(time_series)
    return header, data


def compare(
    ref_point: Point,
    bef_point: Point,
    aft_point: Point,
    delta: float,
    ref_header: Header,
    new_header: Header,
) -> float:
    """Count sum of squares at a given time point."""
    cost = 0.0
    for idx, data in enumerate(ref_point[1:]):
        new_idx = new_header.index(ref_header[idx + 1])
        new_data = delta * bef_point[new_idx] + (1 - delta) * aft_point[new_idx]
        cost += (data - new_data) ** 2
    return cost


def compare_data(reference_file: str, new_file: str) -> float:
    """Read two CSV files and compares by interpolating points in second file."""
    ref_header, [ref_data] = read_data([reference_file])
    ref_header = list(map(lambda x: x.lower(), ref_header))
    new_header, [new_data] = read_data([new_file])
    new_header = list(map(lambda x: x.lower(), new_header))
    cost = 0.0
    after = 0
    for data_point in ref_data:
        time = data_point[0]
        print(time, cost)
        while new_data[after][0] < time:
            after += 1
        after_time = new_data[after][0]
        if after_time == time:
            cost += compare(
                data_point, new_data[after], new_data[after], 0, ref_header, new_header
            )
        else:
            delta = (after_time - time) / (after_time - new_data[after - 1][0])
            cost += compare(
                data_point,
                new_data[after - 1],
                new_data[after],
                delta,
                ref_header,
                new_header,
            )
    return cost


if __name__ == "__main__":
    if len(argv) == 1:
        molecules = "KK,KKK,KKp,KKKp,KKKpKK,KKKpKKp,KKpp"
    else:
        molecules = argv[1]
    gen_biocham_model(molecules.split(","))
