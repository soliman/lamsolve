"""Main entry point."""

from argparse import ArgumentParser, FileType
from sys import stdout

from .lamsolve import solve


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        help="output the LP program as lamsolve.lp in current directory",
    )
    parser.add_argument(
        "-u",
        "--upperbound",
        default=None,
        type=float,
        help="Upper bound of the parameters to be found",
    )
    parser.add_argument(
        "-o",
        "--outfile",
        nargs="?",
        default=stdout,
        type=FileType("w"),
        help="Same as original <odefile> with new parameter values",
    )
    parser.add_argument(
        "-e",
        "--epsilon",
        default=1e-4,
        type=float,
        help="Slope difference limit and slope difference allowance",
    )
    parser.add_argument("odefile", help="XPP ode file as produced by biocham")
    parser.add_argument(
        "datafiles", nargs="+", help="CSV file(s) as produced by biocham"
    )
    solve(**vars(parser.parse_args()))
